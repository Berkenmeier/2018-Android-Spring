package broncos.boisestate.edu.bsuawesome

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        var helloworldTextView:TextView = findViewById(R.id.helloworld_textview)

        helloworldTextView.text = "This is the changed text"
    }
}
